import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  datas = [];

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.getCart();
    let cart = JSON.parse(localStorage.getItem('cart')) || [];
    let total_show = document.getElementById('cart__total');
    let total_value = cart.reduce((a, b) => a + b.price * b.amount, 0);
    total_show.textContent = total_value;
  }
  getCart = () => {
    this.datas = JSON.parse(localStorage.getItem('cart'));
    console.log(this.datas);
  };
  removeItem = (id) => {
    let cart = JSON.parse(localStorage.getItem('cart'));
    let count = document.getElementById('cart__count');
    let updatedCart = cart.filter((item) => {
      return item.id !== id;
    });
    localStorage.setItem('cart', JSON.stringify(updatedCart));
    window.location.reload();
    count.textContent = updatedCart.length;
  };
  removeAllItem = () => {
    let count = document.getElementById('cart__count');
    localStorage.removeItem('cart');
    window.location.reload();
    count.textContent = '0';
  };

  test = (id, event) => {
    let cart = JSON.parse(localStorage.getItem('cart'));
    let updatedCart = cart.map((item) => {
      return item.id === id ? { ...item, amount: +event.target.value } : item;
    });
    localStorage.setItem('cart', JSON.stringify(updatedCart));
    window.location.reload();
  };
}
